package com.ehking.sdk.notify;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson15.JSONObject;
import com.ehking.sdk.HttpClientUtils;

public class NotifyCheckServlet extends HttpServlet {
	static final Logger LOGGER = LoggerFactory.getLogger(NotifyCheckServlet.class);
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String notifyUrl = req.getParameter("notifyUrl");
		String content = req.getParameter("content");
		LOGGER.info("notifyUrl:["+notifyUrl+"]");
		LOGGER.info("content:["+content+"]");
		JSONObject object = JSONObject.parseObject(content);
		LOGGER.info("object:["+object.toJSONString()+"]");
		String responseStr = HttpClientUtils.notify(notifyUrl, object.toJSONString());
		PrintWriter print = resp.getWriter();
		
		print.println("返回:"+responseStr);
	}
}
