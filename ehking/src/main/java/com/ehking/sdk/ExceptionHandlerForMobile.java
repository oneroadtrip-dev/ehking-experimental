package com.ehking.sdk;

import com.alibaba.fastjson15.JSONObject;
import com.ehking.sdk.exception.HmacVerifyException;
import com.ehking.sdk.exception.RequestException;
import com.ehking.sdk.exception.ResponseException;
import com.ehking.sdk.exception.UnknownException;

public abstract class ExceptionHandlerForMobile {
	public static void handle(final JSONObject responseData,Exception e){
		if(e instanceof ResponseException ){
			responseData.put("status", "EXCEPTION");
			responseData.put("exceptionCode", "response.exception");
			responseData.put("message", "响应异常");
		}
		if(e instanceof HmacVerifyException){
			responseData.put("status", "EXCEPTION");
			responseData.put("exceptionCode", "hmac_verify.exception");
			responseData.put("message", "签名验证异常");
		}
		if(e instanceof RequestException){
			responseData.put("status", "EXCEPTION");
			responseData.put("exceptionCode", "request.exception");
			responseData.put("message", "请求异常");
		}
		if(e instanceof UnknownException || e instanceof Exception){
			responseData.put("status", "EXCEPTION");
			responseData.put("exceptionCode", "unknown.exception");
			responseData.put("message", "未知异常");
		}
	}
}
