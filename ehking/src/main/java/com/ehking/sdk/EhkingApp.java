package com.ehking.sdk;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.ehking.sdk.filter.CharacterEncodingFilter;
import com.ehking.sdk.notify.NotifyCheckServlet;
import com.ehking.sdk.onlinepay.HelloServlet;
import com.ehking.sdk.onlinepay.NotifyServlet;
import com.ehking.sdk.onlinepay.OrderServlet;
import com.ehking.sdk.onlinepay.QueryServlet;
import com.ehking.sdk.onlinepay.RefundNotifyServlet;
import com.ehking.sdk.onlinepay.RefundQueryServlet;
import com.ehking.sdk.onlinepay.RefundServlet;

public class EhkingApp {
  private static final Logger LOG = LogManager.getLogger();

  public static class Config {
    @Parameter(names = "--port", description = "Service port", required = false)
    public Integer port = 8081;

    @Parameter(names = { "-h", "--help" }, description = "print help message", required = false)
    public boolean help = false;
  }

  public static void main(String[] args) throws Exception {
    for (String arg : args) {
      LOG.info("arg: {}", arg);
    }
    Config config = new Config();
    JCommander jc = new JCommander(config, args);
    if (config.help) {
      jc.usage();
      return;
    }

    ConfigurationUtils.setConfigSource(new PropertiesConfiguration(
        "src/main/resources/ehking-config.properties"));
    LOG.info("xfguo: key = {}", ConfigurationUtils.getHmacKey("120140257"));

    HandlerCollection handlerCollection = new HandlerCollection();
    {
      ServletContextHandler servletHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
      servletHandler.setContextPath("/onlinepay");
      
      // Hello world test.
      servletHandler.addServlet(new ServletHolder(new HelloServlet()), "/hello");

      // jetty always wants one servlet
//      servletHandler.addServlet(new ServletHolder(new DefaultServlet()), "/*");
      servletHandler.addServlet(new ServletHolder(new QueryServlet()), "/query");
      servletHandler.addServlet(new ServletHolder(new OrderServlet()), "/order");
      servletHandler.addServlet(new ServletHolder(new RefundServlet()), "/refund");
      servletHandler.addServlet(new ServletHolder(new NotifyServlet()), "/notify");
      servletHandler.addServlet(new ServletHolder(new RefundNotifyServlet()), "/refundNotify");
      servletHandler.addServlet(new ServletHolder(new RefundQueryServlet()), "/refundQuery");
      servletHandler.addServlet(new ServletHolder(new NotifyCheckServlet()), "/notifyCheckServlet");

      FilterHolder filterHolder = new FilterHolder(CharacterEncodingFilter.class);
      filterHolder.setInitParameter("characterEncoding", "true");
      servletHandler.addFilter(filterHolder, "/*", EnumSet.allOf(DispatcherType.class));
      
      handlerCollection.addHandler(servletHandler);
    }

    {
      ResourceHandler resourceHandler = new ResourceHandler();
      resourceHandler.setResourceBase("src/main/webapp");
      handlerCollection.addHandler(resourceHandler);
    }

    Server server = new Server(config.port);
    server.setHandler(handlerCollection);

    try {
      LOG.info("staring server...");
      server.start();
      server.join();
    } finally {
      server.destroy();
    }

  }
}
