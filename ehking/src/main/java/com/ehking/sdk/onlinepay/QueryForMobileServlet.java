package com.ehking.sdk.onlinepay;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson15.JSONObject;
import com.ehking.sdk.ExceptionHandlerForMobile;
import com.ehking.sdk.FastJsonUtils;
import com.ehking.sdk.executer.ResultListenerAdpater;
import com.ehking.sdk.onlinepay.builder.QueryBuilder;
import com.ehking.sdk.onlinepay.executer.OnlinePayOrderExecuter;

public class QueryForMobileServlet extends HttpServlet {
	static final Logger LOGGER = LoggerFactory.getLogger(QueryForMobileServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	/**
	 * 请求结构
	 * {
	 * merchantId:,
	 * requestId:
	 * }
	 * 响应结构
	 * {
	 * status:,
	 * message:,
	 * exceptionCode:
	 * responseData:{}
	 */
	@Override
	protected void doPost(HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		
		JSONObject requestJson = FastJsonUtils.convert(req.getInputStream());
		
		String merchantId = requestJson.getString("merchantId");
		String requestId = requestJson.getString("requestId");

		QueryBuilder builder = new QueryBuilder(merchantId);
		builder.setRequestId(requestId);

		JSONObject requestData = builder.build();
		OnlinePayOrderExecuter executer = new OnlinePayOrderExecuter();
		final JSONObject responseData=new JSONObject();
		try{
			executer.query(requestData, new ResultListenerAdpater() {
	            public void success(JSONObject jsonObject) {
	                responseData.put("status", "SUCCESS");
	                responseData.put("message", "支付成功");
	                responseData.put("responseData", jsonObject);
	            }

	            public void failure(JSONObject jsonObject) {
	                responseData.put("status", "FAIL");
	                responseData.put("message", "支付失败");
	                responseData.put("responseData", jsonObject);
	            }
	            
	            public void pending(JSONObject jsonObject) {
	                responseData.put("status", "PENDING");
	                responseData.put("message", "未支付");
	                responseData.put("responseData", jsonObject);
	            }
			});
		}
		catch(Exception e){
			ExceptionHandlerForMobile.handle(responseData, e);
		}

		resp.getOutputStream().write(responseData.toJSONString().getBytes("UTF-8"));  
	    resp.setContentType("text/json; charset=UTF-8");
	}
}
