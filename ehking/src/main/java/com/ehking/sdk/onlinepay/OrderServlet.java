package com.ehking.sdk.onlinepay;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson15.JSONArray;
import com.alibaba.fastjson15.JSONObject;
import com.ehking.sdk.entity.BankCard;
import com.ehking.sdk.entity.Payer;
import com.ehking.sdk.entity.ProductDetail;
import com.ehking.sdk.exception.HmacVerifyException;
import com.ehking.sdk.exception.RequestException;
import com.ehking.sdk.exception.ResponseException;
import com.ehking.sdk.exception.UnknownException;
import com.ehking.sdk.executer.ResultListenerAdpater;
import com.ehking.sdk.onlinepay.builder.OrderBuilder;
import com.ehking.sdk.onlinepay.executer.OnlinePayOrderExecuter;

public class OrderServlet extends HttpServlet {

  private static final long serialVersionUID = -1450599121936722735L;

  static final Logger LOGGER = LoggerFactory.getLogger(OrderServlet.class);

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
    doPost(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, final HttpServletResponse resp)
      throws ServletException, IOException {

    String merchantId = req.getParameter("merchantId");
    String requestId = req.getParameter("requestId");
    String orderAmount = req.getParameter("orderAmount");
    String orderCurrency = req.getParameter("orderCurrency");
    String notifyUrl = req.getParameter("notifyUrl");
    String callbackUrl = req.getParameter("callbackUrl");
    String remark = req.getParameter("remark");
    String email = req.getParameter("email");
    String paymentModeCode = req.getParameter("paymentModeCode");

    // 收银台版本
    String cashierVersion = req.getParameter("cashierVersion");
    // 付款人姓名
    String payerName = req.getParameter("payerName");
    // 手机号码
    String phoneNum = req.getParameter("phoneNum");
    // 证件号
    String idNum = req.getParameter("idNum");
    // 证件类型
    String idType = req.getParameter("idType");
    // 银行卡号
    String bankCardNum = req.getParameter("bankCardNum");
    // 业务背景
    String forUse = req.getParameter("forUse");
    // 商户会员Id
    String merchantUserId = req.getParameter("merchantUserId");
    // 绑卡Id
    String bindCardId = req.getParameter("bindCardId");

    OrderBuilder builder = new OrderBuilder(merchantId);
    builder.setRequestId(requestId).setOrderAmount(orderAmount).setOrderCurrency(orderCurrency)
        .setNotifyUrl(notifyUrl).setCallbackUrl(callbackUrl).setRemark(remark)
        .setPaymentModeCode(paymentModeCode).setNotifyUrl(notifyUrl)
        .setCashierVersion(cashierVersion).setForUse(forUse).setMerchantUserId(merchantUserId)
        .setBindCardId(bindCardId);

    ProductDetail productDetail = new ProductDetail();
    String jsonPDStr = req.getParameter("hiddenPD").trim();

    LOGGER.info("productDetail++++++++++++++" + jsonPDStr);

    JSONArray productDetailArray = JSONObject.parseArray(jsonPDStr);
    for (Object o : productDetailArray) {
      productDetail = JSONObject.parseObject(o.toString(), ProductDetail.class);
      builder.addProductDetail(productDetail);
    }

    Payer payer = new Payer();
    payer.setName(payerName);
    payer.setPhoneNum(phoneNum);
    payer.setBankCardNum(bankCardNum);
    payer.setIdNum(idNum);
    payer.setIdType(idType);
    payer.setEmail(email);

    builder.setPayer(payer);

    BankCard bankCard = new BankCard();
    bankCard.setCardNo(StringUtils.defaultIfEmpty(req.getParameter("cardNo"), null));
    bankCard.setCvv2(StringUtils.defaultIfEmpty(req.getParameter("cvv2"), null));
    bankCard.setExpiryDate(StringUtils.defaultIfEmpty(req.getParameter("expiryDate"), null));
    bankCard.setIdNo(StringUtils.defaultIfEmpty(req.getParameter("idNo"), null));
    bankCard.setMobileNo(StringUtils.defaultIfEmpty(req.getParameter("mobileNo"), null));
    bankCard.setName(StringUtils.defaultIfEmpty(req.getParameter("name"), null));

    builder.setBankCard(bankCard);
    JSONObject requestData = builder.build();

    final PrintWriter out = resp.getWriter();

    OnlinePayOrderExecuter executer = new OnlinePayOrderExecuter();

    try {
      executer.order(requestData, new ResultListenerAdpater() {
        /**
         * 提交成功，不代表支付成功
         */
        public void success(JSONObject jsonObject) {
          LOGGER.debug("success jsonObject:[" + jsonObject + "]");
          out.println("提交成功</br>");
          out.println(jsonObject);
        }

        public void redirect(JSONObject jsonObject, String redirectUrl) throws IOException {
          resp.sendRedirect(redirectUrl);
          LOGGER.debug("redirectUrl:[" + redirectUrl + "]");
        }
      });
    } catch (ResponseException e) {
      out.println("响应异常</br>");
      out.println(e.toString());
    } catch (HmacVerifyException e) {
      out.println("签名验证异常</br>");
      out.println(e.toString());
    } catch (RequestException e) {
      out.println("请求异常</br>");
      out.println(e.toString());
    } catch (UnknownException e) {
      out.println("未知异常</br>");
      out.println(e.toString());
    }
  }
}
