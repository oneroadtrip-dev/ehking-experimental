package com.ehking.sdk.onlinepay;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson15.JSONObject;
import com.ehking.sdk.FastJsonUtils;
import com.ehking.sdk.exception.HmacVerifyException;
import com.ehking.sdk.exception.ResponseException;
import com.ehking.sdk.exception.UnknownException;
import com.ehking.sdk.executer.ResultListenerAdpater;
import com.ehking.sdk.foreignexchange.executer.ForeignExchangeOrderExecuter;

public class RefundNotifyServlet extends HttpServlet {
	static final Logger LOGGER = LoggerFactory.getLogger(RefundNotifyServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    ForeignExchangeOrderExecuter executer = new ForeignExchangeOrderExecuter();
	    
	    try{
		    executer.refundCallback(FastJsonUtils.convert(req.getInputStream()), new ResultListenerAdpater() {
	            
	            public void success(JSONObject jsonObject) {
	                LOGGER.info("支付成功！");
	                LOGGER.info(jsonObject.toJSONString());
	            }
	            
	            public void failure(JSONObject jsonObject) {
	                LOGGER.info("支付失败！");
	                LOGGER.info(jsonObject.toJSONString());
	            }
	        });
	    }
		catch(ResponseException e){
			LOGGER.info("响应异常");
			LOGGER.info(e.toString());
		}
		catch(HmacVerifyException e){
			LOGGER.info("签名验证异常");
			LOGGER.info(e.toString());
		}
		catch(UnknownException e){
			LOGGER.info("未知异常");
			LOGGER.info(e.toString());
		}
	}
}
