package com.ehking.sdk.onlinepay;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;

public class HelloServlet extends HttpServlet {
  private static final long serialVersionUID = -818545962633437729L;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setStatus(HttpStatus.OK_200);
    resp.getWriter().println("Hello world");
  }
}
