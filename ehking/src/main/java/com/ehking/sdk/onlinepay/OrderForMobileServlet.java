package com.ehking.sdk.onlinepay;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson15.JSONArray;
import com.alibaba.fastjson15.JSONObject;
import com.ehking.sdk.ExceptionHandlerForMobile;
import com.ehking.sdk.FastJsonUtils;
import com.ehking.sdk.entity.BankCard;
import com.ehking.sdk.entity.Payer;
import com.ehking.sdk.entity.ProductDetail;
import com.ehking.sdk.executer.ResultListenerAdpater;
import com.ehking.sdk.onlinepay.builder.OrderBuilder;
import com.ehking.sdk.onlinepay.executer.OnlinePayOrderExecuter;

public class OrderForMobileServlet extends HttpServlet {
	static final Logger LOGGER = LoggerFactory.getLogger(OrderForMobileServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	/**
	 * 请求结构
	 * {
	 * merchantId:,
	 * requestId:,
	 * orderAmount:,
	 * orderCurrency:,
	 * notifyUrl:,
	 * callbackUrl:,
	 * remark:,
	 * payerName:,
	 * phoneNum:,
	 * email:,
	 * paymentModeCode:,
	 * contents:{
	 * 				name:,
	 * 				quantity:,
	 * 				amount:,
	 * 				receiver:,
	 * 				description:
	 *          },
	 * cardNo:,
	 * cvv2:,
	 * expiryDate:,
	 * idNo:,
	 * mobileNo:,
	 * name:
	 * }
	 * 响应结构
	 * {
	 * status:,
	 * message:,
	 * exceptionCode:
	 * responseData:{}
	 */
	@Override
	protected void doPost(HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		
		JSONObject requestJson = FastJsonUtils.convert(req.getInputStream());
		
		String merchantId = requestJson.getString("merchantId");
		String requestId = requestJson.getString("requestId");
		String orderAmount = requestJson.getString("orderAmount");
		String orderCurrency = requestJson.getString("orderCurrency");
		String notifyUrl = requestJson.getString("notifyUrl");
		String callbackUrl = requestJson.getString("callbackUrl");
		String remark = requestJson.getString("remark");
		String payerName = requestJson.getString("payerName");
		String phoneNum = requestJson.getString("phoneNum");
		String email = requestJson.getString("email");
		String paymentModeCode = requestJson.getString("paymentModeCode");

		OrderBuilder builder = new OrderBuilder(merchantId);
		builder.setRequestId(requestId).setOrderAmount(orderAmount).setOrderCurrency(orderCurrency)
				.setNotifyUrl(notifyUrl).setCallbackUrl(callbackUrl).setRemark(remark).setPaymentModeCode(paymentModeCode).setNotifyUrl(notifyUrl);

		ProductDetail productDetail = new ProductDetail();
		String jsonStr = requestJson.getString("contents");
		JSONArray array = JSONObject.parseArray(jsonStr);
		for(Object o:array) {
			productDetail = JSONObject.parseObject(o.toString(), ProductDetail.class);
			builder.addProductDetail(productDetail);
		}

		Payer payer = new Payer();
		payer.setName(payerName);
		payer.setPhoneNum(phoneNum);
		payer.setEmail(email);

		builder.setPayer(payer);
		
		BankCard bankCard = new BankCard();
		bankCard.setCardNo(StringUtils.defaultIfEmpty(requestJson.getString("cardNo"), null));
		bankCard.setCvv2(StringUtils.defaultIfEmpty(requestJson.getString("cvv2"), null));
		bankCard.setExpiryDate(StringUtils.defaultIfEmpty(requestJson.getString("expiryDate"), null));
		bankCard.setIdNo(StringUtils.defaultIfEmpty(requestJson.getString("idNo"), null));
		bankCard.setMobileNo(StringUtils.defaultIfEmpty(requestJson.getString("mobileNo"), null));
		bankCard.setName(StringUtils.defaultIfEmpty(requestJson.getString("name"), null));
		
		builder.setBankCard(bankCard);
		JSONObject requestData = builder.build();
		
		OnlinePayOrderExecuter executer = new OnlinePayOrderExecuter();
		final JSONObject responseData=new JSONObject();
		try{
			executer.order(requestData,new ResultListenerAdpater(){
				/**
				 * 提交成功，不代表支付成功
				 */
				public void success(JSONObject jsonObject) {
	                responseData.put("status", "SUCCESS");
	                responseData.put("message", "提交成功");
	                responseData.put("responseData", jsonObject);
				}
				
				public void redirect(JSONObject jsonObject, String redirectUrl) throws IOException{
	                responseData.put("status", "REDIRECT");
	                responseData.put("message", redirectUrl);
	                responseData.put("responseData", jsonObject);
				}
			});
		}
		catch(Exception e){
			ExceptionHandlerForMobile.handle(responseData, e);
		}
		
		resp.getOutputStream().write(responseData.toJSONString().getBytes("UTF-8"));  
	    resp.setContentType("text/json; charset=UTF-8");
	}
}
