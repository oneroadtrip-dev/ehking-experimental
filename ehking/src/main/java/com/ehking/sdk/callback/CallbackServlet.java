package com.ehking.sdk.callback;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CallbackServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		Set set = req.getParameterMap().keySet();
		PrintWriter out = resp.getWriter();
		out.println("回调成功<br>");
		for (Iterator it = set.iterator(); it.hasNext();) {
			String key = it.next().toString();
			out.println(key + ":" + req.getParameter(key));
		}
	}
}
